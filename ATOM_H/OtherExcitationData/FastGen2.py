"""This File was used to Build the xml data for the Aggarwal attomic rates from csv files

"""


import xml
import xml.etree.ElementTree as ET
from XMLIndent import indent
from NEQAIRData.Utils.XMLIndent import indent



root = ET.Element("data")
infile = "ATOM_H/OtherExcitationData/AggarwalPaperRates.csv"
outfile = "ATOM_H/OtherExcitationData/AggarwalPaperRates.xml"

source = "Aggarwal 1990"
sourceshort = "Aggarwal Rates"

with open(infile,'r') as f:
    data = f.readlines()

temps = [ bit.strip() for bit  in data[0].split(',')]

for line in data[1:]:
    vals = line.split(',')
    if len(vals) < 8:
        continue
    

    transitition = ET.SubElement(root,"ElectronImpactExcitation")
    transitition.set("DataType","AveragedCollisionStrength")
    child = ET.SubElement(transitition,"Source")
    child.text = source
    child = ET.SubElement(transitition,"SourceShort")
    child.text = sourceshort
    child = ET.SubElement(transitition,"LowerState")
    child.text = vals[2]
    child = ET.SubElement(transitition,"UpperState")
    child.text = vals[3]
    child = ET.SubElement(transitition,"EnergyChange")
    child.text = vals[1]
    child = ET.SubElement(transitition,"StatisticalWeight")
    child.text = vals[0]
    child = ET.SubElement(transitition,"Data")

    string = ""
    for a,b in zip(temps,vals[4:]):
        string = string + "\n{},{}".format(a,b)
    child.text = string

root = indent(root)
tree = ET.ElementTree(root)
tree.write(outfile)