"""This file was used to build the park atomic excitation rates in xml form.  This required inputing lines.  


It was rather horrible and should not be repeated.
"""




import xml
import xml.etree.ElementTree as ET
from XMLIndent import indent
from NEQAIRData.Utils.XMLIndent import indent



root = ET.Element("data")

#source = input("Source")
source = "Park 1970"
#sourceshort = input("ShortSource")
sourceshort = "Park"
states = input("states:")
#lowerstate = input("lower state ")
#upperstates = input("csv upper states")
states = states.split(',')
lowerstate = states[0]
upperstates = states[1:]
transitionenergies = input("csv energies")
Coeffecients = input("csv coefficients")
expoenents = input("csv exponents")
#upperstates = upperstates.split(',')
transitionenergies = [float(a) for a in transitionenergies.split(',')]
Coeffecients = [float(a) for a in Coeffecients.split(',')]
expoenents = [float(a) for a in expoenents.split(',')]

filename = input("filename")


for state,energy,c,e in zip(upperstates,transitionenergies,Coeffecients,expoenents):
    transitition = ET.SubElement(root,"ElectronImpactExcitation")
    transitition.set("DataType","ParkFit")
    child = ET.SubElement(transitition,"Source")
    child.text = source
    child = ET.SubElement(transitition,"SourceShort")
    child.text = sourceshort
    child = ET.SubElement(transitition,"LowerState")
    child.text = lowerstate
    child = ET.SubElement(transitition,"UpperState")
    child.text = state
    child = ET.SubElement(transitition,"EnergyChange")
    child.text = str(energy)
    child = ET.SubElement(transitition,"Coefficient")
    child.text = str(c)
    child = ET.SubElement(transitition,"Exponent")
    child.text = str(e)

root = indent(root)
tree = ET.ElementTree(root)
tree.write(filename)