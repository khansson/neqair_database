"""This is a horrible run file that holds a bunch of useful function.  Should be broken up.
"""


from NEQAIRData.Atom.AtomStateDefinition import AtomStateDefinition
from NEQAIRData.Atom.Model import AtomModel
import os
import xml.etree.ElementTree as ET
import xml
from NEQAIRData.Molecule.Model import MoleculeModel
from NEQAIRData.CrossSections.CrossSections import ExcitationCrossSectionData


def Basic1():
    print(os.getcwd())
    os.chdir("./ATOM_H/ElectronExcitationData")

    cross_section_data = ExcitationCrossSectionData.BuildFromCSV('ExcitationCrossSection_1.csv')
    print(cross_section_data.source)
    print("{} values".format(len(cross_section_data.ElectonEnergies)))

    therm = ThermalizedCrossSectionData()

    fit = therm.GetRatesFromCrossSections(cross_section_data)

    print(str(fit))
    print(str(fit[0]))

    print("coeffiecient: {}, exponent: {}".format(therm.Coeffecient,therm.Exponent))

    for temp,k in zip(therm.electron_temperatures,therm.rates):
        print("{},{}".format(temp,k))


def MakeCrossSectionPlot(DataObjs):
    with open("CrossSecionData.dat","w") as f:
        f.write("TITLE = \"Cross Section Data\"\n")
        f.write("VARIABLES = \"Energy (eV)\" \"Cross Section (m<sup>2</sup>)\"\n")
        for obj in DataObjs:
            if not obj.HasCrossSectionData:
                continue
            f.write("ZONE I={}, DataPACKING=POINT, T=\"{}\"\n".format(len(obj.CrossSectionData.ElectonEnergies),obj.name))
            for e,sig in zip(obj.CrossSectionData.ElectonEnergies,obj.CrossSectionData.CrossSections):
                f.write("{},{}\n".format(e,sig))


def MakeRatesPlot(DataObjs):
    with open("RateData.dat","w") as f:
        f.write("TITLE = \"Cross Section Data\"\n")
        f.write("VARIABLES = \"Temperature (K)\" \"10000 / Temperature\" \"Rate (cm<sup>-3</sup>s<sup>-1</sup>)\" \n")
        for obj in DataObjs:
            f.write("ZONE I={}, DataPACKING=POINT, T=\"{} - Fit\"\n".format(len(range(1000,20000,1000)),obj.name))
            for temp in range(1000,20000,1000):
                inverse = 10000/temp
                rate = obj.Coeffecient * (temp/10000)** obj.Exponent * np.exp(-obj.ThresholdEnergy * ElectronCharge / (temp * BolzmanConstant))
                f.write("{},{},{}\n".format(temp,inverse,rate))
            if not obj.HasRates:
                continue
            f.write("ZONE I={},DATAPACKING=POINT,T=\"{}\"\n".format(len(obj.electron_temperatures),obj.name))
            for temp,rate in zip(obj.electron_temperatures,obj.rates):
                f.write("{},{},{}\n".format(temp,10000.0/temp,rate*1e6))




def Basic2():
    Data = []


    for root, dirs, files in os.walk("../ATOM_H/OtherExcitationData"):
        for file in files:
            if file.endswith(".xml"):
                filename = os.path.join(root, file)
                print("reading : " + filename)
                tree = ET.parse(filename)
                base = tree.getroot()
                for node in base.findall("ElectronImpactExcitation"):
                    Data.append(ThermalizedCrossSectionData.BuildFromXML(node))


    for bit in Data:
        print("---------------------------")
        for temp,rate in zip(bit.electron_temperatures,bit.rates):
            print("{},{}".format(temp,rate))

    for bit in Data:
        if not bit.HasCrossSectionData:
            continue
        print("*******************************")
        for e,sig in zip(bit.CrossSectionData.ElectonEnergies,bit.CrossSectionData.CrossSections):
            print("{},{}".format(e,sig))


    for bit in Data:
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print("{}: {},{}".format(bit.name,bit.Coeffecient,bit.Exponent))

    newdata = []

    lowerstate = AtomStateDefinition.FromString("2")
    upperstate = AtomStateDefinition.FromString("3")

    for bit in Data:
        if lowerstate.ContainsAll(bit.level1) and upperstate.ContainsAll(bit.level2):
            newdata.append(bit)

    MakeCrossSectionPlot(newdata)
    MakeRatesPlot(newdata)


def Basic3():
    atom = AtomModel()
    atom.name="H"
    atom.Read_Excite_Atoms("../DATABASES/EXCITE_ATOMS.dat")
    atom.WriteCompositeLevelsXML("CompositeLevels.xml")

def Basic4():
    atom = AtomModel()
    atom.name="H"
    atom.ReadCompositeLevelsXML("ATOM_H/CompositeLevels.xml")
    atom.ReadExcitationDataXML("ATOM_H/ElectronExcitationData/")
    print(atom.NEQAIRCompositeString())
    print(atom.NEQAIRExcitationString())

def Basic5():
    molecule = MoleculeModel()
    molecule.name = "H2"
    molecule.ReadLEVELS_MOLE("./DATABASES/LEVELS_MOLE.dat")
    molecule.WriteLevelXML("Levels.xml")
    molecule.ReadFrankCondonFactors("./MOLE_H2/FrankCondonFactors")
    for transition in molecule.transitions:
        if not transition.HaveFrankCondon:
            continue
        string = transition.FrankCondonSet.WriteNEQAIRString()
        print(string + "\n\n\n")


def Basic6():
    molecule = MoleculeModel()
    molecule.ReadMolecularData("./MOLE_H2")
    molecule.ReadLevels("./MOLE_H2/Levels")
    molecule.ReadFrankCondonFactors("./MOLE_H2/FrankCondonFactors")
    molecule.ReadEinsteinCoefficents("./MOLE_H2/EinsteinCoefficents")
    molecule.ReadDissociationCrossSections("./MOLE_H2/DissociationCrossSections")
    molecule.ReadDissociationRates("./MOLE_H2/DissociationRates")
    molecule.ReadElectronicExcitations("./MOLE_H2/ElectronExcitationData")
    molecule.ReadQuenchingRates("./MOLE_H2/QuenchingRates")
    string = molecule.ExciteMoleculeNEQAIRString()
    a = input("Ready for tring ? (press enter)")
    print(string)

if __name__ == "__main__":
    Basic6()
