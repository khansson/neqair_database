


from NEQAIRData.CrossSections.CrossSections import ThermalizedCrossSectionData
import xml.etree.ElementTree as ET

def Run():
    filename = "./MOLE_H2/IonizationData/Jacobsen1995.xml"

    tree = ET.parse(filename)
    base = tree.getroot()
    node = base.find("ElectronImpactExcitation")

    DATA = ThermalizedCrossSectionData.BuildFromXML(node)

    fit = DATA.MakeArrheniusFit()

    print(fit)
    for t,k in zip(DATA.electron_temperatures,DATA.rates):
        print("{},{}".format(t,k))



if __name__ == "__main__":
    Run()







