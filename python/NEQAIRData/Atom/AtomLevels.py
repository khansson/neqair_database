import xml.etree.ElementTree as ET
from .AtomStateDefinition import AtomStateDefinition

class Level:
    def __init__(self):
        self.index = 0
        self.multiplicity
        self.energy = 0
        self.name = AtomStateDefinition()

    @classmethod
    def FromString(self,inputString):
        """Reads NEQAIR strings from LEVELS_ATOMS.dat"""
        self = self()

        data = inputString.split()
        self.index = int(data[0])
        self.multiplicity = int(data[1])
        self.energy = float(data[2])
        
        if len(data) == 5:
            self.name = AtomStateDefinition.FromString(data[3] + " " + data[4])
                
        return self



class CompositeLevel:
    def __init__(self):
        self.index=0
        self.principal_quantum_number = 0
        self.energy = 0
        self.degeneracy = 0
        self.levels = []

    def ContainsSet(self,levels):
        return all([ any([comp_level.Contains(rate_level) for comp_level in self.levels])  for rate_level in levels])


    @classmethod
    def FromString(self,inputString):
        """Reads NEQAIR strings from EXCITE_ATOMS.dat"""
        self = self()

        data = inputString.split()
        self.index = int(data[0])
        self.principal_quantum_number = int(data[1])
        self.energy = float(data[2])
        self.degeneracy = int(float(data[3]))
        
        
        if len(data) > 4:
            StateData = data[4:]
            if len(StateData) == 1:
                self.levels.append(AtomStateDefinition.FromPrincipal(int(StateData[0])))
            else:
                if (len(StateData)%2 == 1):
                    print("error in reading EXCITE_ATOMS.dat")
                    return 1
                for first, second in zip(StateData[::2], StateData[1::2]):
                    self.levels.append(AtomStateDefinition.FromString(first + " " + second))

        return self

    def XMLNode(self):
        node = ET.Element("CompositeLevel")
        node.set("index",str(self.index))
        node.set("energy",str(self.energy))
        node.set("degeneracy",str(self.degeneracy))
        node.set("principalQuantumNumber",str(self.principal_quantum_number))
        child = ET.SubElement(node,"Levels")
        string = ','.join([level.WriteString() for level in self.levels])
        child.text = string
        return node
        
    def ReadXMLNode(self,node):
        self.index = int(node.get("index"))
        self.principal_quantum_number = int(node.get("principalQuantumNumber"))
        self.energy = float(node.get("energy"))
        self.degeneracy = int(node.get("degeneracy"))
        child = node.find("Levels")
        self.levels = [AtomStateDefinition.FromString(string.strip()) for string in child.text.split(',')]

    @classmethod
    def FromXMLNode(self,node):
        self = self()
        self.ReadXMLNode(node)
        return self

    def NEQAIRString(self):
        string = "{:4d}{:4d}{:9.0f}.{:9.0f}. ".format(self.index,self.principal_quantum_number,self.energy,self.degeneracy)
        string2 = ' '.join([level.WriteString() for level in self.levels])
        return string + string2

    @staticmethod
    def NEQAIREndString():
        string = "{:4d}{:4d}{:9.0f}.{:9.0f}. ".format(-1,0,0,0)
        return string