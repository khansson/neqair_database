from ..Utils.Constants import *



class AtomStateDefinition:
    def __init__(self):
        self.outer_shell_principal_number = 0
        self.outer_shell_momentum = 0
        self.number_free_electons = 0
        self.have_free_electrons = False
        self.spin_multiplicity = 0
        self.configuration_angular_momentum = 0
        self.OnlyPrincipal = False

    
    OrbitNumberFromChar = {
        "s":0,
        "S":0,
        "p":1,
        "P":1,
        "d":2,
        "D":2,
        "f":3,
        "F":3,
        "n":-1,
        "N":-1,
        "x":-2,
        "X":-2
    }

    OrbitCharFromNumber = {
        -2:"X",
        -1:"n",
        0:"S",
        1:"P",
        2:"D",
        3:"F"
    }
       

    @classmethod
    def FromString(self,inputString):
        if len(inputString) == 0 or len(inputString) == 1:
            return self.FromPrincipal(int(inputString))
        self = self()

        
        string = inputString.strip()
        index = 0
        startindex = 0
        while(string[index].isnumeric()):
            index += 1
            if (index == len(string)):
                break
        self.outer_shell_principal_number = int(string[startindex:index])
        if(index == len(string)):
            self.OnlyPrincipal = True
            return self
        self.outer_shell_momentum = self.OrbitNumberFromChar.get(string[index])
        
        index +=1
        if (index >= len(string)):  #These are the same thing, but i wanted to be explicit
            self.OnlyPrincipal = True
            return self
        startindex = index
        while( string[index].isnumeric()):
            index += 1

        if (index != startindex):
            self.number_free_electons = int(string[startindex:index])
            self.have_free_electrons = True

        while( not string[index].isnumeric()):
            index += 1

        startindex = index
        while( string[index].isnumeric()):
            index += 1
        self.spin_multiplicity = int(string[startindex:index])
        self.configuration_angular_momentum = self.OrbitNumberFromChar.get(string[index])


        return self

    @classmethod
    def FromPrincipal(self,PrincipalQuantumNumber):
        self = self()
        self.outer_shell_principal_number = PrincipalQuantumNumber
        self.OnlyPrincipal = True
        return self

    def WriteString(self):
        if(self.OnlyPrincipal):
            return str(self.outer_shell_principal_number)
        Principal = str(self.outer_shell_principal_number)
        AngularMomentum = self.OrbitCharFromNumber.get(self.outer_shell_momentum)
        if(self.have_free_electrons):
            FreeElections = str(self.number_free_electons)
        else:
            FreeElections = ""
        Spin = str(self.spin_multiplicity)
        Config = self.OrbitCharFromNumber.get(self.configuration_angular_momentum)

        return "{}{}{} {}{}".format(Principal,AngularMomentum,FreeElections,Spin,Config)

    def Contains(self,otherState):
        if not (self.outer_shell_principal_number == otherState.outer_shell_principal_number):
            return False

        if self.OnlyPrincipal:
            return True

        if self.outer_shell_momentum !=  otherState.outer_shell_momentum and self.outer_shell_momentum != -1:
            return False
        if self.spin_multiplicity != otherState.spin_multiplicity:
            return False

        if  self.configuration_angular_momentum == -2:
            return True

        if self.configuration_angular_momentum != otherState.configuration_angular_momentum:
            return False

        if not self.have_free_electrons:
            return True
        
        if not otherState.have_free_electrons:
            return False
        
        return self.have_free_electrons == otherState.have_free_electrons

    def ContainsAll(self,statelist):
        return all([self.Contains(a) for a in statelist])
        

        


