

from .AtomLevels import *
from ..CrossSections.CrossSections import ThermalizedCrossSectionData
import xml.etree.ElementTree as ET
import os
from ..Utils.XMLIndent import indent

class AtomModel:
    def __init__(self):
        self.levels = []
        self.composite_levels = []
        self.lines = []
        self.ExitationRates = []
        self.name = ""
        

    def Read_Levels_Atoms(self,file_location):
        NameFromDashDistance = 2
        DataFromNameDistance = 4
        if not "LEVELS_ATOMS.dat" in file_location:
            print("Warning: This file doesn't look like the LEVELS_ATOMS.dat NEQAIR database" )

        with open(file_location,'r') as f:
            datafile = f.readlines()

        AtomFound = False
        StartIndex = 0
        for i in range(len(datafile)):
            if datafile[i][0] == '-':
                i = i + NameFromDashDistance
                if self.name in datafile[i]:
                    AtomFound = True
                    StartIndex = i
                    break

        if not AtomFound:
            print("atom " + self.name + "was not found in file" + file_location)
            return 1


        StartIndex = i + DataFromNameDistance
        EndIndex = StartIndex

        while len(datafile[EndIndex]) != 0:
            self.levels.append(Level.FromString(datafile[EndIndex])) #generate class from data string
            EndIndex = EndIndex + 1
            #This may fail


        return 0
        



    def Read_Excite_Atoms(self,file_location):
        NameFromDashDistance = 2
        DataFromNameDistance = 2
        LevelDataToExcitationDataDistance = 6
        ColumnLengthOfExitationData = 19
        if not "EXCITE_ATOMS.dat" in file_location:
            print("Warning: This file doesn't look like the LEVELS_ATOMS.dat NEQAIR database" )

        with open(file_location,'r') as f:
            datafile = f.readlines()



        AtomFound = False
        StartIndex = 0
        for i in range(len(datafile)):
            if datafile[i][0] == '*':
                i = i + NameFromDashDistance
                if self.name == datafile[i].strip():
                    AtomFound = True
                    StartIndex = i
                    break

        if not AtomFound:
            print("atom " + self.name + "was not found in file" + file_location)
            return 1

        index = StartIndex + DataFromNameDistance

        #Read Composite Levels
        while (index < len(datafile)):
            line = datafile[index]
            splitline = line.split()
            if(int(splitline[0]) == -1):
                break

            self.composite_levels.append(CompositeLevel.FromString(line)) 
            index = index + 1   

        index = index + LevelDataToExcitationDataDistance
        #Read Thermal Exitation Rates
        n = ColumnLengthOfExitationData 
        self.ExitationRates = []
        while (index < len(datafile)):
            line = datafile[index]
            splitline = line.split()
            if(int(splitline[0]) == -1):
                break

            for chunk in [line[i:i+n] for i in range(0, len(line), n)]:  #n defined just above, which is really defined at start
                #TODO: PULL OUT DATA
                if len(chunk) < 19:
                    continue
                data = ThermalizedCrossSectionData()
                
                level1 = int(chunk[0:4])
                level2 = int(chunk[4:7])
                data.Coeffecient = float(chunk[6:14])
                data.Exponent = float(chunk[14:19])
                self.ExitationRates.append(data)

            
            index = index + 1

    def ReadCompositeLevelsXML(self,filename):
        tree = ET.parse(filename)
        base = tree.getroot()
        self.composite_levels = [CompositeLevel.FromXMLNode(node) for node in base.findall("CompositeLevel")]


    def WriteCompositeLevelsXML(self,filename):
        root = ET.Element("data")
        for composite in self.composite_levels:
            root.append(composite.XMLNode())
        root = indent(root)
        tree = ET.ElementTree(root)
        tree.write(filename)


    def ReadExcitationDataXML(self,directory):
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("ElectronImpactExcitation"):
                        self.ExitationRates.append(ThermalizedCrossSectionData.BuildFromXML(node))

    def NEQAIRCompositeString(self):
        return "\n".join([composite.NEQAIRString() for composite in self.composite_levels]+[CompositeLevel.NEQAIREndString()])

    def NEQAIRExcitationString(self):
        strings = []
        maxlower = 0
        maxupper = 0
        for i in range(len(self.composite_levels)):
            for j in range(i+1,len(self.composite_levels)):
                lowerComposite = self.composite_levels[i]
                upperComposite = self.composite_levels[j]
                rates = []
                for rate in self.ExitationRates:
                    if lowerComposite.ContainsSet(rate.level1) and upperComposite.ContainsSet(rate.level2):
                        rates.append(rate)

                if len(rates) == 0:
                    print("no rates between {} and {}".format(lowerComposite.index,upperComposite.index))
                    continue
                if len(rates) == 1:
                    FinalRate = rates[0]
                else:
                    print("Warning: Multiple excitation rates refer to the same pain of composite levels, combination not yet implemented")
                    FinalRate = rates[0]

                maxupper = max(upperComposite.index,maxupper)
                maxlower = max(maxlower,lowerComposite.index)

                string = "{:3d}{:3d}{:8.1E}{:5.2f}".format(lowerComposite.index,upperComposite.index,FinalRate.Coeffecient,FinalRate.Exponent)
                strings.append(string)

        
        LineIndex = 1
        levelsPerLine = 4
        lines = []
        i = 1
        for block in [strings[i:i+levelsPerLine] for i in range(0, len(strings), levelsPerLine)]:
            while len(block) < 4:
                block.append("{:3d}{:3d}{:8.1E}{:5.2f}".format(maxlower,maxupper+i,0,0))
                i+=1
            line = ''.join(block)
            line = line + "{:3d}".format(LineIndex)
            LineIndex += 1
            lines.append(line)

        finalline = "-1  0 0.0     0.0   0  0 0.0     0.0   0  0 0.0     0.0   0  0 0.0     0.0"

        lines.append(finalline)

        finalstring = '\n'.join(lines)

        return finalstring
        

        
        