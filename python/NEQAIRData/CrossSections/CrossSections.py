"""This is the object containers to read and write the neqair database to/from 
a format that is more user friendly (i.e. csv)

This is just for atoms, unclear if I will generalize
"""

import numpy as np
import scipy.integrate as integrate
import scipy.optimize as optimize
from ..Utils.Constants import *
from ..Atom.AtomStateDefinition import AtomStateDefinition
import xml




class ExcitationCrossSectionData:
    """This is varius data from sources"""
    def __init__(self):
        self.name1 = ""
        self.name2 = ""
        self.source = ""
        self.ElectonEnergies = []
        self.CrossSections = []
        self.ThresholdEnergy = 0
        self.isAtom = True
        self.label = ""

    def ReadStateName(self,text):
        if self.isAtom:
            return AtomStateDefinition.FromString(text)
        else:
            return text
    
    def ReadCSV(self,inputfile):
        with open(inputfile,mode='r') as f:
            datafile = f.readlines()
        
        for line in datafile:
            splitline = line.split(',')
            if (splitline[0] == "Level 1"):
                self.ReadStateName(self.name1,splitline[1])
            if (splitline[0] == "Level 2"):
                self.ReadStateName(self.name2,splitline[1])
            if (splitline[0] == "Source"):
                self.source = ','.join(splitline[1:])
            if (splitline[0] == "Threshold Energy"):
                self.ThresholdEnergy = float(splitline[1])
            if (is_number_tryexcept(splitline[0])):
                self.ElectonEnergies.append(float(splitline[0]))
                self.CrossSections.append(float(splitline[1]))

    def ReadCrossSectionNode(self,node):
        self.name1 = self.ReadStateName(node.find("LowerState").text)
        self.name2 = self.ReadStateName(node.find("UpperState").text)
        self.source = node.find("Source").text
        self.label = node.find("SourceShort").text
        self.ThresholdEnergy = float(node.find("EnergyChange").text)
        UnitNode = node.find("Units")
        data = node.find("Data").text
        data = data.splitlines()
        data = [line.split(',') for line in data]
        for line in data:
            if(len(line) <2):
                continue
            self.ElectonEnergies.append(float(line[0]))
            self.CrossSections.append(float(line[1]))
        #TODO DEAL WITH UNITS HERE SET TO EV BY M2   
        if UnitNode.get("CrossSection") == "A2":
            self.CrossSections = [a * 1e-20 for a in self.CrossSections]

    def ReadCollisionStrengthNode(self,node):
        self.ReadStateName(self.name1,node.find("LowerState").text)
        self.ReadStateName(self.name2,node.find("UpperState").text)
        self.source = node.find("Source").text
        self.label = node.find("SourceShort").text
        self.ThresholdEnergy = float(node.find("EnergyChange").text)
        statisticalweight = float(node.find("StatisticalWeight").text)
        UnitNode = node.find("Units")
        data = node.find("Data").text
        data = data.splitlines()
        data = [line.split(',') for line in data]
        collisionStrength = 0.0
        energy = 0.0
        for line in data:
            if(len(line) <2):
                continue
            energy = float(line[0])
            self.ElectonEnergies.append(energy)
            if not UnitNode.get("Energy") in UnitNode.get("CollisionStrength"):
                print("need some logic here")
            collisionStrength = float(line[1])
            self.CrossSections.append(collisionStrength / (energy * statisticalweight))
        if UnitNode.get("Energy") == "Ryd":
            self.ElectonEnergies = [a * RydToEV for a in self.ElectonEnergies]
        if "Bohr" in UnitNode.get("CollisionStrength"):
            self.CrossSections = [a * BohrCrossSection for a in self.CrossSections]
        

    def ReadXMLNode(self,node):
        if (node.get("DataType") == "CrossSection"):
            self.ReadCrossSectionNode(node)
            return 0
        if (node.get("DataType") == "CollisionStrength"):
            self.ReadCollisionStrengthNode(node)
            return  0
        print("ERROR, cannot read node")
        return 1

    @staticmethod
    def CanReadXMLNode(node):
        if (node.get("DataType") == "CrossSection"):
            return True
        if (node.get("DataType") == "CollisionStrength"):
            return  True
        return False

    @classmethod
    def BuildFromCSV(self,inputfile):
        self = self()
        self.ReadCSV(inputfile)
        return self

    @classmethod
    def BuildFromXML(self,node,AsAtom = True):
        self = self()
        self.isAtom = AsAtom
        self.ReadXMLNode(node)
        return self



class ThermalizedCrossSectionData:
    def __init__(self):
        self.level1 = []
        self.level2 = []
        self.Coeffecient = 0.0
        self.Exponent = 0.0
        self.HasRates = False
        self.rates = []
        self.electron_temperatures = []
        self.HasCrossSectionData = False
        self.CrossSectionData = 0
        self.ThresholdEnergy = 0.0
        self.name = ""

    

    def diffRate(self,temperature,energy,cross_section):
        expoentialTerm = np.exp(-energy*ElectronCharge/(BolzmanConstant * temperature))
        sqrtTerm =  (np.pi * MassElectron * (BolzmanConstant * temperature)**3) ** -0.5
        return 2.0 * cross_section * energy * ElectronCharge ** 2 * sqrtTerm * expoentialTerm 

    def GetSingleRate(self,Temperature,Energies,CrossSections):
        diff_rate = [self.diffRate(Temperature,energy,crossSection) for energy,crossSection in zip(Energies, CrossSections)]
        rate = integrate.simps(diff_rate,Energies)
        return rate

    def GetRatesFromCrossSections(self,CrossSectionData):
        if not CrossSectionData.isAtom:
            raise Exception
        self.HasRates = True
        self.electron_temperatures = range(1000,50000,500)
        self.rates = []
        self.level1 = [CrossSectionData.name1]
        self.level2 = [CrossSectionData.name2]
        self.ThresholdEnergy = CrossSectionData.ThresholdEnergy
        for temp in self.electron_temperatures:
            self.rates.append(self.GetSingleRate(temp,CrossSectionData.ElectonEnergies,CrossSectionData.CrossSections))


    def MakeFit(self):
        x = [t/10000 for t in self.electron_temperatures]
        y = [k/np.exp(-self.ThresholdEnergy * ElectronCharge/(BolzmanConstant * t)) for k,t in zip(self.rates,self.electron_temperatures)]

        def form(x,c,n):
            return c*x ** n 

        fit = optimize.curve_fit(form,x,y)
        self.Coeffecient = fit[0][0] * 1e6
        self.Exponent = fit[0][1]

        return fit

    @staticmethod
    def Arrhenius(Temperature,Coeffecient,Exponent,CharacteristicTemp):
        bit = Coeffecient * Temperature ** Exponent * np.exp(-CharacteristicTemp / Temperature)
        return bit


    def ArrheniusError(self,vals):
        Coeffecient,Exponent,CharacteristicTemp = vals
        error = 0
        norm = np.average(self.electron_temperatures)
        for k,t in zip(self.rates,self.electron_temperatures):
            bit = t*(np.log10(k) - np.log10(self.Arrhenius(t,Coeffecient,Exponent,CharacteristicTemp)))
            error += bit **2



        return error/norm**2

    def MakeArrheniusFit(self):

        guess=[1e-7,-1,self.ThresholdEnergy*EVtoK]
        
        final = optimize.minimize(self.ArrheniusError,guess,method='Nelder-Mead',options={ 'maxiter': 20000, 'adaptive': True})  #,

        return final



    def ReadAveragedCollisionStrengthNode(self,node):
        
        self.HasRates = True
        self.level1 = [AtomStateDefinition.FromString(node.find("LowerState").text)]
        self.level2 = [AtomStateDefinition.FromString(node.find("UpperState").text)]
        self.source = node.find("Source").text
        self.ThresholdEnergy = float(node.find("EnergyChange").text)
        UnitNode = node.find("Units")
        statisticalweight = float(node.find("StatisticalWeight").text)
        data = node.find("Data").text
        data = data.splitlines()
        data = [line.split(',') for line in data]
        collisionStrength = 0.0
        temperature = 0.0
        for line in data:
            if(len(line) <2):
                continue
            temperature = float(line[0])
            self.electron_temperatures.append(temperature)
            collisionStrength = float(line[1])
            self.rates.append(collisionStrength *0.00000863 /1e6 * temperature ** (-0.5) / statisticalweight * np.exp(-self.ThresholdEnergy * ElectronCharge/ (temperature * BolzmanConstant)))
        self.MakeFit()

    def ReadParkFitNode(self,node):
        self.HasRates = False
        self.level1 = [AtomStateDefinition.FromString(node.find("LowerState").text)]
        self.level2 = [AtomStateDefinition.FromString(node.find("UpperState").text)]
        self.source = node.find("Source").text
        self.ThresholdEnergy = float(node.find("EnergyChange").text)
        UnitNode = node.find("Units")
        self.Coeffecient = float(node.find("Coefficient").text)
        self.Exponent = float(node.find("Exponent").text)

    def ReadXMLNode(self,node):
        if (node.get("DataType") == "AveragedCollisionStrength"):
            self.name = node.find("SourceShort").text
            return self.ReadAveragedCollisionStrengthNode(node)

        if ExcitationCrossSectionData.CanReadXMLNode(node):
            self.name = node.find("SourceShort").text
            self.HasCrossSectionData = True
            self.CrossSectionData = ExcitationCrossSectionData.BuildFromXML(node)
            self.GetRatesFromCrossSections(self.CrossSectionData)
            self.MakeFit()
        
        

        if (node.get("DataType") == "ParkFit"):
            self.name = node.find("SourceShort").text
            return self.ReadParkFitNode(node)


    @classmethod
    def BuildFromXML(self,node):
        self = self()
        self.ReadXMLNode(node)
        return self  


            

        
if __name__ == "__main__":
    
    print("This is a test")

