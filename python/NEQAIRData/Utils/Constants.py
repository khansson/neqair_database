import numpy as np

BolzmanConstant = 1.3806485e-23 #m2 kg s-2 K-1
MassElectron = 9.1093835e-31 # kilograms
ElectronCharge = 1.60217662e-19 
BohrRadius = 5.293e-11  
BohrCrossSection = np.pi * BohrRadius ** 2
RydToEV = 13.605662285137
AvagadrosNumber = 6.02214076e23
M2ToCm2 = 10000
EVtoK = 1.160451812e4
ElectronMolarMass = 5.4857990888e-4 #gram/mol
gPerkg = 1000


def is_number_tryexcept(s):
    """ Returns True is string is a number. """
    try:
        float(s)
        return True
    except ValueError:
        return False