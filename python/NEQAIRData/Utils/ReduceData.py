



def ReduceData(xdata,ydata,numberofpoints):
    indexList = []

    for i in range(len(ydata)):
        indexList.append([i,0])

    while(len(indexList) > numberofpoints):
        for i in range(1,len(indexList)-1):
            startindex = indexList[i-1][0]
            endindex = indexList[i+1][0]
            differences = []
            for j in range(indexList[i-1][0] + 1 , indexList[i+1][0]):                      
                realativeIncrease = (xdata[j] - xdata[startindex]) / (xdata[endindex] - xdata[startindex])
                CrossSectionDiff = ydata[endindex] - ydata[startindex]
                OringalValue = ydata[j]
                InterpolatedValue = ydata[startindex] +  CrossSectionDiff *  realativeIncrease
                differences.append( (OringalValue - InterpolatedValue) ** 2 )
            indexList[i][1] = sum(differences) / len(differences)

        popindex = 1
        for i in range(2 , len(indexList) -1):
            if indexList[i][1] < indexList[popindex][1]:
                popindex = i

        indexList.pop(popindex)

    finalYData = []
    finalXData = []
    for bit in indexList:
        finalYData.append(ydata[bit[0]])
        finalXData.append(xdata[bit[0]])

    return finalXData,finalYData

if __name__ == "__main__":
    pass