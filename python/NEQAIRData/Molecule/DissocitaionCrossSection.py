
from ..Utils.XMLIndent import indent
from ..Utils.ReduceData import ReduceData
from ..Utils.Constants import M2ToCm2

class DissociationCrossSection:
    def __init__(self):
        self.energies = []
        self.cross_sections = []
        self.state = ""
        self.source = ""
        self.name = ""

    def ReadXMLNode(self, node, nodeParent):
        self.source = node.find("Source").text
        self.name = node.find("SourceShort").text
        self.state = node.find("State").text
        energyshift = 0
        dataNode = node.find("Data")
        EnergyChange = float(node.find("EnergyChange").text)
        unitnode = node.find("Units")
        scalefactor = 1

        if dataNode.text.strip() == "UsePrevious":
            nodeset = nodeParent.findall('DissociationCrossSection')
            for i in range(len(nodeset)):
                if node == nodeset[i]:
                    break

            for j in range(i-1,-1,-1):
                if nodeset[j].find('Data').text.strip() != "UsePrevious":
                    dataNode = nodeset[j].find('Data')
                    energyshift = EnergyChange - float(nodeset[j].find("EnergyChange").text)
                    unitnode = nodeset[j].find("Units")

        for line in dataNode.text.strip().split():
            if "cm" in unitnode.get("CrossSection"):
                scalefactor = 1e-4
            bits = line.split(',')
            self.energies.append(float(bits[0]) + energyshift)
            self.cross_sections.append(scalefactor * float(bits[1]))

    @classmethod
    def BuildFromXML(self,node,parentNode):
        self = self()
        self.ReadXMLNode(node,parentNode)
        return self

    def GetFinalValues(self):
        finalCrossSections = []
        finalEnergies = []
        NumNeqairValues = 8
        if len(self.cross_sections) == NumNeqairValues:
            finalCrossSections = self.cross_sections
            finalEnergies = self.energies
        elif len(self.cross_sections) < 8:
            NumNeeded = NumNeqairValues - len(self.cross_sections)
            finalCrossSections = self.cross_sections
            finalEnergies = self.energies
            for i in range(NumNeeded):
                finalCrossSections.insert(1,(finalCrossSections[1] - finalCrossSections[0])/2)
                finalEnergies.insert(1,(finalEnergies[1] - finalEnergies[0])/2)
        else:
            finalEnergies,finalCrossSections = ReduceData(self.energies,self.cross_sections,8)
            
        return finalEnergies, finalCrossSections


    def NEQAIRString(self):
        finalEnergies, finalCrossSections = self.GetFinalValues()
        string1 = "".join(["{:10.3E}".format(energy) for energy in finalEnergies])
        string2 = "".join(["{:10.3E}".format(cs*M2ToCm2) for cs in finalCrossSections])

        return string1 + "\n" + string2