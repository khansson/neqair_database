


class EinsteinCoefficent:
    def __init__(self):
        self.value = 0
        self.source = ""
        self.name = ""
        self.upperElectronicState=""
        self.lowerElectronicState=""

    def ReadXMLNode(self,node):
        self.source = node.find("Source").text
        self.name = node.find("SourceShort").text
        self.upperElectronicState = node.find("UpperState").text
        self.lowerElectronicState = node.find("LowerState").text
        self.value = float(node.find("Data").text.strip())

    @classmethod
    def BuildFromXML(self,node):
        self = self()
        self.ReadXMLNode(node)
        return self

    def NEQAIRString(self):
        return "{:9.3E}      (Effective Einstein A coef. from {})".format(self.value,self.name)