from .FrankCondonFactors import FrankCondonFactorSet
from .EinsteinCoefficent import EinsteinCoefficent
from ..CrossSections.CrossSections import ExcitationCrossSectionData
from ..Utils.ReduceData import ReduceData
from ..Utils.Constants import M2ToCm2


class MolecularTransition:
    def __init__(self):
        self.FrankCondonSet = ""
        self.HaveFrankCondon = False
        self.ElectionCrossSections = ""
        self.HaveElectionCrossSections = False
        self.EinsteinCoefficient = ""
        self.HaveEinsteinCoefficient = False
        self.upperElectronicState=""
        self.upperElectronicStateIndex = 0
        self.lowerElectronicState=""
        self.lowerElectronicStateIndex = 0
        self.name = "BandName"


    def CrossSectionNEQAIRString(self):
        finalEnergies,finalCrossSections = ReduceData(self.ElectionCrossSections.ElectonEnergies,self.ElectionCrossSections.CrossSections,8)
        string = ("        CROSS-SECTIONS FROM: {}\n".format(self.ElectionCrossSections.label) +
        " eV1        eV2       eV3       eV4       eV5       eV6       eV8       eV9\n"+
        "cr-sec     cr-sec    cr-sec    sc-sec    cr-sec    cr-sec    cr-sec    cr-sec\n"+
        "".join(["{:10.3E}".format(energy) for energy in finalEnergies]) + "\n" + 
        "".join(["{:10.3E}".format(cs * M2ToCm2) for cs in finalCrossSections]) + "\n")

        return string

    def NEQAIRString(self):
        string = ("\n" + 
        "      Franck-Condon factors (qvv), electron excitation rate coef's and\n"+
        "      effective Einstein A coef's for transitions between states in QSS.\n"+
        "Note: Franck-Condon factor data are terminated by -1 for first vu in line.\n"+
        "\n"+
        "  Name        l    u (states)\n"+
        " {}       {}    {}\n".format(self.name,self.lowerElectronicStateIndex,self.upperElectronicStateIndex) + 
        self.FrankCondonSet.WriteNEQAIRString() +
        "\n"+
        self.CrossSectionNEQAIRString() +
        "\n"+
        self.EinsteinCoefficient.NEQAIRString())

        return string