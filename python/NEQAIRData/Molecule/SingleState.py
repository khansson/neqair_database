
from .DissocitaionCrossSection import DissociationCrossSection

class MolecularState:
    def __init__(self):
        self.name = ""
        self.index = 0


        self.vdata = 0
        self.vlimit = 0
        self.degeneracy = 0
        self.ElectronicTemperature = 0
        self.Requil = 0
        self.Dzero = 0
        self.A_SpinOrbit = 0
        self.Lambda = 0
        self.we = 0
        self.wexe = 0
        self.weye = 0
        self.weze = 0
        self.weae = 0
        self.webe = 0
        self.wece = 0
        self.Be = 0
        self.alphae =0
        self.De = 0
        self.betae = 0
        self.gammaSpinSpin = 0
        self.epsSpinSpin = 0
        self.HaveDissociationCrossSection = False
        self.DissociationCrossSection = ""
        self.comments = ""

    def ReadLEVELS_MOLE_line(self,line):
        data=line.split()
        self.index = int(data[0])
        self.name = data[1]
        self.vdata = int(data[2])
        self.vlimit = int(data[3])
        self.degeneracy = int(float(data[4]))
        self.ElectronicTemperature = float(data[5])
        self.Requil = float(data[6])
        self.Dzero = float(data[7])
        self.A_SpinOrbit = float(data[8])
        self.Lambda = float(data[9])
        self.we = float(data[10])
        self.wexe = float(data[11])
        self.weye = float(data[12])
        self.weze = float(data[13])
        self.weae = float(data[14])
        self.webe = float(data[15])
        self.wece = float(data[16])
        self.Be = float(data[17])
        self.alphae =float(data[18])
        self.De = float(data[19])
        self.betae = float(data[20])
        self.gammaSpinSpin = float(data[21])
        self.epsSpinSpin = float(data[22])
        self.comments = ' '.join(data[23:])



    @classmethod
    def BuildFromLEVELS_MOLE_line(self,line):
        self = self()
        self.ReadLEVELS_MOLE_line(line)
        return self

    
    def XMLNode(self):
        node = ET.Element("MolecularState")
        node.set("index",str(self.index))

        def MakeDataTextNode(name,value):
            child = ET.SubElement(node,name)
            child.text = str(value)
            return child

        MakeDataTextNode("LevelName",self.name)
        MakeDataTextNode("vdata",self.vdata)
        MakeDataTextNode("vlimit",self.vlimit)
        MakeDataTextNode("degeneracy",self.degeneracy)
        MakeDataTextNode("Requil",self.Requil)
        MakeDataTextNode("Dzero",self.Dzero)
        MakeDataTextNode("ElectronicTemperature",self.ElectronicTemperature)
        MakeDataTextNode("A_SpinOrbit",self.A_SpinOrbit)
        MakeDataTextNode("Lambda",self.Lambda)
        MakeDataTextNode("we",self.we)
        MakeDataTextNode("wexe",self.wexe)
        MakeDataTextNode("weye",self.weye)
        MakeDataTextNode("weze",self.weze)
        MakeDataTextNode("weae",self.weae)
        MakeDataTextNode("webe",self.webe)
        MakeDataTextNode("wece",self.wece)
        MakeDataTextNode("Be",self.Be)
        MakeDataTextNode("alphae",self.alphae)
        MakeDataTextNode("De",self.De)
        MakeDataTextNode("betae",self.betae)
        MakeDataTextNode("gammaSpinSpin",self.gammaSpinSpin)
        MakeDataTextNode("epsSpinSpin",self.epsSpinSpin)
        MakeDataTextNode("Comments",self.comments)

        return node

    def ReadXMLNode(self,node):

        self.index = int(node.get("index"))

        def ReadDataTextNodeFloat(name):
            child = node.find(name)
            value = float(child.text)
            return value

        def ReadDataTextNodeString(name):
            child = node.find(name)
            value = child.text
            return value

        def ReadDataTextNodeInt(name):
            child = node.find(name)
            value = int(child.text)
            return value

        self.name = ReadDataTextNodeString("LevelName")
        self.vdata = ReadDataTextNodeInt("vdata")
        self.vlimit = ReadDataTextNodeInt("vlimit")
        self.degeneracy = ReadDataTextNodeInt("degeneracy")
        self.Requil = ReadDataTextNodeFloat("Requil")
        self.Dzero = ReadDataTextNodeFloat("Dzero")
        self.ElectronicTemperature = ReadDataTextNodeFloat("ElectronicTemperature")
        self.A_SpinOrbit = ReadDataTextNodeFloat("A_SpinOrbit")
        self.Lambda = ReadDataTextNodeFloat("Lambda")
        self.we = ReadDataTextNodeFloat("we")
        self.wexe = ReadDataTextNodeFloat("wexe")
        self.weye = ReadDataTextNodeFloat("weye")
        self.weze = ReadDataTextNodeFloat("weze")
        self.weae = ReadDataTextNodeFloat("weae")
        self.webe = ReadDataTextNodeFloat("webe")
        self.wece = ReadDataTextNodeFloat("wece")
        self.Be = ReadDataTextNodeFloat("Be")
        self.alphae = ReadDataTextNodeFloat("alphae")
        self.De = ReadDataTextNodeFloat("De")
        self.betae = ReadDataTextNodeFloat("betae")
        self.gammaSpinSpin = ReadDataTextNodeFloat("gammaSpinSpin")
        self.epsSpinSpin = ReadDataTextNodeFloat("epsSpinSpin")
        self.comments = ReadDataTextNodeString("Comments")

    @classmethod
    def BuildFromXML(self,node):
        self = self()
        self.ReadXMLNode(node)
        return self

    def ElectronDisociationString(self):
        string = "State number {}: Te={}\n".format(self.index,self.ElectronicTemperature) 
        return string + self.DissociationCrossSection.NEQAIRString()