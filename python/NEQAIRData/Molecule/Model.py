import xml.etree.ElementTree as ET
import os
from ..CrossSections.CrossSections import ExcitationCrossSectionData
import numpy as np
import scipy
from ..Utils.ReduceData import ReduceData

from ..Utils.XMLIndent import indent

from .SingleState import MolecularState
from .Transition import MolecularTransition
from .FrankCondonFactors import FrankCondonFactorSet
from .DissocitaionCrossSection import DissociationCrossSection
from .EinsteinCoefficent import EinsteinCoefficent
from .QuenchingRates import QuenchingRate
from .DissocitationRates import DissociationRateSet

class MoleculeModel:
    def __init__(self):
        self.name = ""
        self.DissocitaionProducts = []
        self.numberElectonStates = 0
        self.molecularWeight = 0
        self.reducedMass = 0
        self.enthaplyFormation = 0
        self.states = []
        self.transitions = []
        self.DissociationRates = []
        self.QuenchingRates = []

    def ReadLEVELS_MOLE(self,filename):
        NameToStateDataDistance = 3
        with open(filename,'r') as f:
            datafile = f.readlines()
        
        MoleculeFound = False
        index = 0
        line = ''
        data = []
        for index in range(len(datafile)):
            line = datafile[index]
            data = line.split()
            if len(data) == 0:
                continue
            if data[0].strip() == self.name:
                MoleculeFound = True
                break
        if not MoleculeFound:
            print("Molecule not Found")
            return

        self.numberElectonStates = int(data[1])
        self.molecularWeight = float(data[2])
        self.reducedMass = float(data[3])
        self.enthaplyFormation = float(data[4])

        StartIndex = index + NameToStateDataDistance

        for index in range(StartIndex,StartIndex + self.numberElectonStates):
            self.states.append(MolecularState.BuildFromLEVELS_MOLE_line(datafile[index]))

    def ReadEXCITE_MOLE(self,filename):

        with open(filename,'r') as f:
            datafile = f.readlines()


    def WriteLevelXML(self,filename):
        root = ET.Element("data")
        for state in self.states:
            root.append(state.XMLNode())
        root = indent(root)
        tree = ET.ElementTree(root)
        tree.write(filename)

    
    def ReadLevels(self,directory):
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("MolecularState"):
                        self.states.append(MolecularState.BuildFromXML(node))

        def getindex(state):
            return state.index
        self.states.sort(key = getindex)

    def ReadMolecularXMLNode(self,node):
        self.name = node.find("Name").text
        for productnode in node.find("DissocitationProducts").findall("Product"):
            self.DissocitaionProducts.append(productnode.text)
        self.molecularWeight = float(node.find("MolecularWeight").text)
        self.reducedMass = float(node.find("ReducedMass").text)
        self.enthaplyFormation = float(node.find("EnthalpyFormation").text)

    def ReadMolecularData(self,directory):
        for file in os.listdir(directory):
            if file.endswith(".xml"):
                filename = os.path.join(directory,file)
                print("reading : " + filename)
                tree = ET.parse(filename)
                base = tree.getroot()
                for node in base.findall("MoleculeModel"):
                    self.ReadMolecularXMLNode(node)
                    return
                

    
    def ReadFrankCondonFactors(self,directory):
        Sets = []
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("FrankCondonFactors"):
                        Sets.append(FrankCondonFactorSet.BuildFromXML(node))

        for FCset in Sets:
            FoundTranitions = False
            for transition in self.transitions:
                if transition.HaveFrankCondon:
                    continue
                if transition.upperElectronicState == FCset.upperElectronicState and transition.lowerElectronicState == FCset.lowerElectronicState:
                    transition.FrankCondonSet = FCset
                    transition.HaveFrankCondon = True
                    FoundTranitions = True
                    break
            if not FoundTranitions:
                newtrans = MolecularTransition()
                newtrans.FrankCondonSet = FCset
                newtrans.HaveFrankCondon = True
                newtrans.upperElectronicState = FCset.upperElectronicState
                newtrans.lowerElectronicState = FCset.lowerElectronicState
                self.transitions.append(newtrans)

    def ReadElectronicExcitations(self,directory):
        Sets = []
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("ElectronImpactExcitation"):
                        Sets.append(ExcitationCrossSectionData.BuildFromXML(node,AsAtom = False))

        for EEset in Sets:
            FoundTransitions = False
            for transition in self.transitions:
                if transition.HaveElectionCrossSections:
                    continue
                if transition.upperElectronicState == EEset.name2 and transition.lowerElectronicState == EEset.name1:
                    transition.ElectionCrossSections = EEset
                    transition.HaveElectionCrossSections = True
                    FoundTransitions = True
                    break
            if not FoundTransitions:
                newtrans = MolecularTransition()
                newtrans.ElectionCrossSections = EEset
                newtrans.HaveElectionCrossSections = True
                newtrans.upperElectronicState = EEset.name1
                newtrans.lowerElectronicState = EEset.name2
                self.transitions.append(newtrans)


    def ReadEinsteinCoefficents(self,directory):
        Sets = []
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("EinteinCoefficent"):
                        Sets.append(EinsteinCoefficent.BuildFromXML(node))

        for ECset in Sets:
            FoundTransitions = False
            for transition in self.transitions:
                if transition.HaveEinsteinCoefficient:
                    continue
                if transition.upperElectronicState == ECset.upperElectronicState and transition.lowerElectronicState == ECset.lowerElectronicState:
                    transition.EinsteinCoefficient = ECset
                    transition.HaveEinsteinCoefficient = True
                    FoundTransitions = True
                    break
            if not FoundTransitions:
                newtrans = MolecularTransition()
                newtrans.EinsteinCoefficient = ECset
                newtrans.HaveEinsteinCoefficient = True
                newtrans.upperElectronicState = ECset.upperElectronicState
                newtrans.lowerElectronicState = ECset.lowerElectronicState
                self.transitions.append(newtrans)


    def ReadDissociationCrossSections(self,directory):
        Sets = []
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("DissociationCrossSection"):
                        Sets.append(DissociationCrossSection.BuildFromXML(node,base))

        
        for Dset in Sets:
            for state in self.states:
                if state.name == Dset.state and not state.HaveDissociationCrossSection:
                    state.DissociationCrossSection = Dset
                    state.HaveDissociationCrossSection = True
                    break
            

    def ReadDissociationRates(self,directory):
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("DissociationRate"):
                        self.DissociationRates.append(DissociationRateSet.BuildFromXML(node))


    def ReadQuenchingRates(self,directory):
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".xml"):
                    filename = os.path.join(root, file)
                    print("reading : " + filename)
                    tree = ET.parse(filename)
                    base = tree.getroot()
                    for node in base.findall("QuenchingRate"):
                        self.QuenchingRates.append(QuenchingRate.BuildFromXML(node,self))

    def FinalDisscoiationRate(self):
        atomcount = 0
        molcount = 0
        sametempandexponent = True
        for rateset in self.DissociationRates:
            if rateset.parterIsMolecule:
                molcount += 1
            else:
                atomcount += 1
            if rateset.exponent != self.DissociationRates[0].exponent or rateset.temperature != self.DissociationRates[0].temperature:
                sametempandexponent = False

        if atomcount == 1 and molcount == 1 and sametempandexponent:
            for rateset in self.DissociationRates:
                if rateset.parterIsMolecule:
                    molrate = rateset.coefficent
                else:
                    atomrate = rateset.coefficent

            exponent = self.DissociationRates[0].exponent
            temperature = self.DissociationRates[0].temperature
            return atomrate, molrate, exponent, temperature

        temperatureRange = list(range(10000,50000,1000))

        def error(inputVals):
            atomrate,molrate,exponent,temperature = inputVals
            error = 0
            for rateset in self.DissociationRates:
                for temp in temperatureRange:
                    rate = rateset.EvalAtTemp(temp)
                    fitrate = temp**exponent * np.exp(-temperature / temp)
                    if rateset.parterIsMolecule:
                        fitrate *= molrate 
                    else:
                        fitrate *= atomrate
                    error += (fitrate - rate) ** 2
            return error ** 0.5

        guess = [self.DissociationRates[0].coefficent,self.DissociationRates[0].coefficent,self.DissociationRates[0].exponent,self.DissociationRates[0].temperature]
        fitResult = scipy.optimize.minimize(error, guess)
        atomrate,molrate,exponent,temperature = fitResult.x

        return atomrate,molrate,exponent,temperature
        

    
    def DisscoiationRateNEQAIRString(self):
        atomrate,molrate,exponent,temperature = self.FinalDisscoiationRate()
        string = ("Dissociation Rates\n" + 
        "      Total Dissociation follows ktot=A*T**n*exp(-Eact) [cm3/molec-s]\n"+
        "     State specific rate follows either ki = ktot*exp(-Di/T)/exp(-D1/T) or\n"+
        "	  	ki = ktot*qtot*exp(-Di/T)/sum(qevr(i)*exp(-Di/T)), depending on neq_qss_hdiss variable\n" +
        "A (atoms) A (molec)    n	Eact (K)\n"+
        "{}   {}   {}   {}".format(atomrate,molrate,exponent,temperature))

        return string

    def ConvertStringToIndex(self, string):
        for state in self.states:
            if string == state.name:
                return state.index
        return "X"



    def GetTransitionIndecies(self):
        for transition in self.transitions:
            transition.upperElectronicStateIndex = self.ConvertStringToIndex(transition.upperElectronicState)
            transition.lowerElectronicStateIndex = self.ConvertStringToIndex(transition.lowerElectronicState)
        
            
    def CommentsString(self):
        ratelist = []
        for rateset in self.DissociationRates:
            if rateset.name not in ratelist:
                ratelist.append(rateset.name)
        
        CrossSectionList = []

        for state in self.states:
            if state.HaveDissociationCrossSection:
                if state.DissociationCrossSection.name not in CrossSectionList:
                    CrossSectionList.append(state.DissociationCrossSection.name)
            

        return "\n".join(
            [
                "Comments and references for {}".format(self.name),
                "",
                "Dissociation Rates are taken from: " + " ".join(ratelist),
                "Electron Dissociation Cross Sections are taken from: " + " ".join(CrossSectionList),

            ]
        )

    def ExciteMoleculeNEQAIRString(self):
        dashline = "--------------------------------------------------------------------------------"
        starline = "********************************************************************************"
        FinalString = [ self.CommentsString(),
            starline,
            "{}".format(self.name), 
        dashline,
        "" ,
        "     Dissociation products and electron impact dissociation cross sections", 
        "                   for electronic states included in QSS.\n" +
        "",
        "{}".format(self.DissocitaionProducts[0]),
        "{}".format(self.DissocitaionProducts[1]),
        " CROSS-SECTIONS sources can be found in NEQAIR Database Gen Repositort",
        " eV1        eV2       eV3       eV4       eV5       eV6       eV8       eV9",
        " cr-sec     cr-sec    cr-sec    sc-sec    cr-sec    cr-sec    cr-sec    cr-sec"]

        for state in self.states:
            if not state.HaveDissociationCrossSection:
                break
            FinalString.append("\n" + state.ElectronDisociationString())

        if len(self.QuenchingRates) > 0:
            FinalString.append(dashline)
            FinalString += QuenchingRate.NEQAIRIntroStrings
            for Qrate in self.QuenchingRates:
                FinalString.append(Qrate.NEQAIRString())

        FinalString.append(dashline)

        self.GetTransitionIndecies()

        FinalString.append(self.DisscoiationRateNEQAIRString())

        

        for transition in self.transitions:
            if transition.HaveFrankCondon and transition.HaveElectionCrossSections and transition.HaveEinsteinCoefficient:
                FinalString.append(dashline)
                FinalString.append(transition.NEQAIRString())
            elif not transition.HaveFrankCondon:
                print("Transition from {} ({}) to {} ({}) is missing frank condon factors".format(transition.lowerElectronicState,transition.lowerElectronicStateIndex,transition.upperElectronicState,transition.upperElectronicStateIndex))
            elif not transition.HaveElectionCrossSections:
                print("Transition from {} ({}) to {} ({}) is missing electronic exctation cross sections".format(transition.lowerElectronicState,transition.lowerElectronicStateIndex,transition.upperElectronicState,transition.upperElectronicStateIndex))
            elif not transition.HaveEinsteinCoefficient:
                print("Transition from {} ({}) to {} ({}) is missing Einstein Coefficents".format(transition.lowerElectronicState,transition.lowerElectronicStateIndex,transition.upperElectronicState,transition.upperElectronicStateIndex))

        FinalString.append(starline)

        FinalString = "\n".join(FinalString)

        return FinalString
                


        








    
