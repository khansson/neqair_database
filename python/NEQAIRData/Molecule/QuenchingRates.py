import numpy as np
from ..Utils.Constants import *

from ..Utils.ReducedMass import ReducedMass


class QuenchingRate:

    NEQAIRTemperature = 300 
    NEQAIRDensity = 1.225

    def __init__(self):
        self.source = ""
        self.name = ""
        self.upperElectronicState=""
        self.lowerElectronicState=""
        self.Rate = 0
        self.Partner = ""
        self.ActivationEnergy = 0
        self.upperElectronicStateIndex = 0
        self.lowerElectronicStateIndex = 0

    def ReadAverageRatesNode(self,node):
        datanode = node.find("Data")
        unitnode = node.find("Units")
        temp = float(datanode.find("Temperature").text)
        self.ActivationEnergy = 0
        scalefactor  = 1.0

        if temp != self.NEQAIRTemperature:
            scalefactor *= np.sqrt(300/temp)

        rates = []

        for node in datanode.findall("Rate"):
            rates.append(float(node.text) * scalefactor)

        self.Rate = sum(rates) / len(rates)

    def ReadAverageCrossSectionNode(self,node,MoleculeModel):
        datanode = node.find("Data")
        rates = []
        unitnode = node.find("Units")
        scalefactor = 1
        if "cm2" == unitnode.get("CrossSection"):
            scalefactor = M2ToCm2

        Mass = MoleculeModel.molecularWeight #ReducedMass(MoleculeModel.molecularWeight,ElectronMolarMass)

        self.ActivationEnergy = 0

        for CSnode in datanode.findall("CrossSection"):
            crosssection = float(CSnode.text) / scalefactor
            rate = crosssection * 4 * np.sqrt(BolzmanConstant * self.NEQAIRTemperature * AvagadrosNumber * gPerkg / (np.pi * Mass)) 
            rates.append(rate)

        self.Rate = sum(rates) / len(rates)


    def ReadXMLNode(self,node,MoleculeModel):
        self.source = node.find("Source").text
        self.name = node.find("SourceShort").text
        self.upperElectronicState = node.find("UpperState").text
        self.lowerElectronicState = node.find("LowerState").text
        self.upperElectronicStateIndex = MoleculeModel.ConvertStringToIndex(self.upperElectronicState)
        self.lowerElectronicStateIndex = MoleculeModel.ConvertStringToIndex(self.lowerElectronicState)
        self.Partner = node.find("Partner").text
        if self.Partner == "None" or self.Partner is None:
            self.Partner = ""

        datanode = node.find("Data")

        if datanode.get('type') == "RateByUpperVibrationalNumber":
            self.ReadAverageRatesNode(node)

        if datanode.get('type') == "ThermallyAveragedCrossSection":
            self.ReadAverageCrossSectionNode(node,MoleculeModel)

    @classmethod
    def BuildFromXML(self,node,MoleucleMode):
        self = self()
        self.ReadXMLNode(node,MoleucleMode)
        return self


    def NEQAIRString(self):
        string =   "{}	   {}         {} 		{}    	   	{}	! From {}".format(self.upperElectronicStateIndex,self.lowerElectronicStateIndex,self.Rate * 100**3 ,self.ActivationEnergy,self.Partner,self.name)
        return string

    NEQAIRIntroStrings = ["Quenching Rates",
"	  Quenching is assumed to follow k=A*sqrt(T/300)*exp(-Ea/T)",
"	  Format as upper, lower, rate, Ea, Partner",
"	  Levels may be given as ranges - rate is divided evenly amongst lower levels",
"Upper    Lower   Rate @ 300K (cm3/s)	Eact (K)  Partner"]