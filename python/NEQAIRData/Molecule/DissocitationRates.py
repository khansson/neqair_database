
from ..Utils.Constants import AvagadrosNumber

class DissociationRateSet:
    def __init__(self):
        self.coefficent = 0
        self.exponent = 0
        self.temperature = 0
        self.parterIsMolecule = False
        self.source = ""
        self.name = ""

    def ReadXMLNode(self,node):
        self.source = node.find("Source").text
        self.name = node.find("SourceShort").text

        partner = node.find("Partner").text

        if any([a.isnumeric() for a in partner]) or sum(1 for c in partner if c.isupper()) > 1:
            self.parterIsMolecule = True


        datanode = node.find('Data')
        unitnode = node.find('Units')

        scalefactor = 1.0
        if unitnode.get("Rate") == "m3/kmol/s":
            scalefactor =   100 ** 3 /  ( 1000 * AvagadrosNumber)

        if datanode.get('type') == "arrhenius":
            self.coefficent = scalefactor * float(datanode.find("Coefficient").text)
            self.exponent = float(datanode.find("Exponent").text)
            self.temperature = float(datanode.find("Temperature").text)

    def EvalAtTemp(self,temperature):
        """Calculated the 

        Args:
            temperature ([type]): [description]

        Returns:
            [type]: [description]
        """
        return self.coefficent * temperature ** self.exponent * np.exp(- self.temperature / temperature)

    @classmethod
    def BuildFromXML(self,node):
        self = self()
        self.ReadXMLNode(node)
        return self