

class FrankCondonFactorSet:
    def __init__(self):
        self.factors = []
        self.source = ""
        self.name = ""
        self.upperElectronicState=""
        self.lowerElectronicState=""


    def WriteNEQAIRString(self):
        levelsPerLine = 5
        firststring = " vu vl    qvv    vu vl    qvv    vu vl    qvv    vu vl    qvv    vu vl    qvv"
        strings = [firststring]
        for block in [self.factors[i:i+levelsPerLine] for i in range(0, len(self.factors), levelsPerLine)]:
            strings.append(''.join([factor.WriteNEQAIRString() for factor in block]))
        laststring =  "-1  0 0.0        0  0 0.0        0  0 0.0        0  0 0.0        0  0 0.0"
        strings.append(laststring)
        final = "\n".join(strings) + "\n"


        return final

    def readCSVtext(self,text):
        data = text.split("\n")
        upperIndex = 0
        lowerIndex = 0

        for line in data:
            if len(line) == 0:
                continue
            lowerIndex = 0
            splitline = line.split(',')
            for value in splitline:
                self.factors.append(FrankCondonFactor.BuildFromData(upperIndex,lowerIndex,float(value)))
                lowerIndex += 1
            upperIndex += 1


    def ReadFrankCondonXMLNode(self,node):
        self.source = node.find("Source").text
        self.name = node.find("SourceShort").text
        self.upperElectronicState = node.find("UpperState").text
        self.lowerElectronicState = node.find("LowerState").text
        data = node.find("Data")
        

        if (data.get("type") == "CSVUpperStateRows"):
            self.readCSVtext(data.text)

    @classmethod
    def BuildFromXML(self,node):
        self = self()
        self.ReadFrankCondonXMLNode(node)
        return self



class FrankCondonFactor:
    def __init__(self):
        self.UpperVibrational = 0
        self.LoweVibrational = 0
        self.FrankCondonFactor = 0

    @classmethod
    def BuildFromData(self,upper,lower,FCFactor):
        self = self()
        self.UpperVibrational = upper
        self.LoweVibrational = lower
        self.FrankCondonFactor = FCFactor
        return self

    def WriteNEQAIRString(self):
        string = "{:3d}{:3d}{:10.3E}".format(self.UpperVibrational,self.LoweVibrational,self.FrankCondonFactor)
        return string
