import unittest
from AtomStateDefinition import AtomStateDefinition

class TestStringMethods(unittest.TestCase):

    def test_ReadString(self):
        string="2p2 4S"
        asd = AtomStateDefinition.FromString(string)
        self.assertEqual(asd.outer_shell_principal_number,2)
        self.assertEqual(asd.outer_shell_momentum,1)
        self.assertEqual(asd.number_free_electons,2)
        self.assertEqual(asd.spin_multiplicity,4)
        self.assertEqual(asd.configuration_angular_momentum,0)
        self.assertEqual(asd.have_free_electrons,True)
        self.assertEqual(asd.OnlyPrincipal,False)

    def test_ReadString2(self):
        string="2p 4S"
        asd = AtomStateDefinition.FromString(string)
        self.assertEqual(asd.outer_shell_principal_number,2)
        self.assertEqual(asd.outer_shell_momentum,1)
        self.assertEqual(asd.spin_multiplicity,4)
        self.assertEqual(asd.configuration_angular_momentum,0)
        self.assertEqual(asd.have_free_electrons,False)
        self.assertEqual(asd.OnlyPrincipal,False)

    def test_ReadString3(self):
        string="2n 4X"
        asd = AtomStateDefinition.FromString(string)
        self.assertEqual(asd.outer_shell_principal_number,2)
        self.assertEqual(asd.have_free_electrons,False)
        self.assertEqual(asd.OnlyPrincipal,False)

    def test_ReadString4(self):
        string="2"
        asd = AtomStateDefinition.FromString(string)
        self.assertEqual(asd.outer_shell_principal_number,2)
        self.assertEqual(asd.have_free_electrons,False)
        self.assertEqual(asd.OnlyPrincipal,True)

    def test_WriteString(self):
        string="2P2 4S"
        asd = AtomStateDefinition.FromString(string)
        out = asd.WriteString()
        self.assertEqual(string,out)

    def test_Contains(self):
        string1="2P2 4S"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2P2 4S"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertTrue(asd1.Contains(asd2))
        self.assertTrue(asd2.Contains(asd1))

        string1="2P2 4S"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2P 4S"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertFalse(asd1.Contains(asd2))
        self.assertTrue(asd2.Contains(asd1))
    
        string1="2P2 4S"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2n 2X"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertFalse(asd1.Contains(asd2))
        self.assertFalse(asd2.Contains(asd1))

        string1="2P2 2S"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2n 2X"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertFalse(asd1.Contains(asd2))
        self.assertTrue(asd2.Contains(asd1))

        string1="2"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2n 2X"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertTrue(asd1.Contains(asd2))
        self.assertFalse(asd2.Contains(asd1))

        string1="3"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2n 2X"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertFalse(asd1.Contains(asd2))
        self.assertFalse(asd2.Contains(asd1))

        string1="2n 3X"
        asd1 = AtomStateDefinition.FromString(string1)
        string2 = "2n 2X"
        asd2 = AtomStateDefinition.FromString(string2)
        self.assertFalse(asd1.Contains(asd2))
        self.assertFalse(asd2.Contains(asd1))

    

if __name__ == '__main__':
    unittest.main()