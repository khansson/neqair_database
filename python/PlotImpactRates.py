from CrossSections import *
from AtomStateDefinition import AtomStateDefinition
from Atom import AtomModel
import os
import xml.etree.ElementTree as ET
import xml
from Molecule import *

def MakeCrossSectionPlot(DataObjs,string):
    filename = "CrossSecionData_{}.dat".format(string)
    with open(filename,"w") as f:
        f.write("TITLE = \"Cross Section Data\"\n")
        f.write("VARIABLES = \"Energy (eV)\" \"Cross Section (m<sup>2</sup>)\"\n")
        for obj in DataObjs:
            if not obj.HasCrossSectionData:
                continue
            f.write("ZONE I={}, DataPACKING=POINT, T=\"{}\"\n".format(len(obj.CrossSectionData.ElectonEnergies),obj.name))
            for e,sig in zip(obj.CrossSectionData.ElectonEnergies,obj.CrossSectionData.CrossSections):
                f.write("{},{}\n".format(e,sig))


def MakeRatesPlot(DataObjs,string):
    filename = "RateData_{}.dat".format(string)
    with open(filename,"w") as f:
        f.write("TITLE = \"Cross Section Data\"\n")
        f.write("VARIABLES = \"Temperature (K)\" \"10000 / Temperature\" \"Rate (cm<sup>-3</sup>s<sup>-1</sup>)\" \n")
        for obj in DataObjs:
            f.write("ZONE I={}, DataPACKING=POINT, T=\"{} - Fit\"\n".format(len(range(1000,20000,1000)),obj.name))
            for temp in range(1000,20000,1000):
                inverse = 10000/temp
                rate = obj.Coeffecient * (temp/10000)** obj.Exponent * np.exp(-obj.ThresholdEnergy * ElectronCharge / (temp * BolzmanConstant))
                f.write("{},{},{}\n".format(temp,inverse,rate))
            if not obj.HasRates:
                continue
            f.write("ZONE I={},DATAPACKING=POINT,T=\"{}\"\n".format(len(obj.electron_temperatures),obj.name))
            for temp,rate in zip(obj.electron_temperatures,obj.rates):
                f.write("{},{},{}\n".format(temp,10000.0/temp,rate*1e6))




def PlotRates(lowerStateString,upperStateString):
    Data = []


    for root, dirs, files in os.walk("../ATOM_H/OtherExcitationData"):
        for file in files:
            if file.endswith(".xml"):
                filename = os.path.join(root, file)
                print("reading : " + filename)
                tree = ET.parse(filename)
                base = tree.getroot()
                for node in base.findall("ElectronImpactExcitation"):
                    Data.append(ThermalizedCrossSectionData.BuildFromXML(node))


    """ for bit in Data:
        print("---------------------------")
        for temp,rate in zip(bit.electron_temperatures,bit.rates):
            print("{},{}".format(temp,rate))

    for bit in Data:
        if not bit.HasCrossSectionData:
            continue
        print("*******************************")
        for e,sig in zip(bit.CrossSectionData.ElectonEnergies,bit.CrossSectionData.CrossSections):
            print("{},{}".format(e,sig))


    for bit in Data:
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print("{}: {},{}".format(bit.name,bit.Coeffecient,bit.Exponent)) """

    newdata = []

    lowerstate = AtomStateDefinition.FromString(lowerStateString)
    upperstate = AtomStateDefinition.FromString(upperStateString)

    for bit in Data:
        if lowerstate.ContainsAll(bit.level1) and upperstate.ContainsAll(bit.level2):
            newdata.append(bit)
    tag = "{}_to_{}".format(lowerStateString,upperStateString)
    MakeCrossSectionPlot(newdata,tag)
    MakeRatesPlot(newdata,tag)

    for bit in newdata:
        print(bit.name + " " + str(bit.Exponent))


if __name__ == "__main__":
    PlotRates("2","4")